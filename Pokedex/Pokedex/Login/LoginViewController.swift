//
//  LoginViewController.swift
//  Pokedex
//
//  Created by Juan Sulca on 4/16/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var pokedexImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //pokedexImageView.image = UIImage(named: "img1")
        // Do any additional setup after loading the view.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let db = Firestore.firestore()
        
        guard let currentUser = Auth.auth().currentUser else {
            return
        }
        
        activityIndicator.startAnimating()
        activityIndicator.isHidden = false
        
        let userRef = db.collection("users").document(currentUser.uid)
        userRef.getDocument { (snapshot, error) in
            self.activityIndicator.stopAnimating()
            if error  != nil || snapshot?.get("registerCompleted") == nil {
                self.performSegue(withIdentifier: "toRegisterSegue", sender: self)
            } else {
                self.performSegue(withIdentifier: "toMainSegue", sender: self)
            }
        }
    }
    
    
    @IBAction func loginButtonPressed(_ sender: UIButton, forEvent event: UIEvent) {
        /*let mail = "juan@mail.com"
        let password = "#@@@"
        
        if (mail, password) == (emailTextField.text, passwordTextField.text) {
            performSegue(withIdentifier: "toMainView", sender: self)
            return
        }
        
        showAlertError()
        */
        firebaseAuth(email: emailTextField.text!, passwd: passwordTextField.text!)
    }
    
    
    @IBAction func createAccountButton(_ sender: UIButton, forEvent event: UIEvent) {
    }
    
    
    func showAlertError(){
        let alertView = UIAlertController(title: "Error", message: "Please enter valid credentials!!", preferredStyle: .alert)
        let okAlertAction = UIAlertAction(title: "OK", style: .default) { [unowned self](_) in
            self.emailTextField.text = ""
            self.passwordTextField.text = ""
        }
        alertView.addAction(okAlertAction)
        present(alertView, animated: true, completion: nil)
    }
    
    func firebaseAuth(email: String, passwd: String) {
        Auth.auth().signIn(withEmail: email, password: passwd){(result, error) in
            if let _ = error {
                self.showAlertError()
                return
            }
            self.performSegue(withIdentifier: "toMainView", sender: self)
        }
    }
}
