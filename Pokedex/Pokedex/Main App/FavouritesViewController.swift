//
//  FavouritesViewController.swift
//  Pokedex
//
//  Created by Juan Sulca on 6/18/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import RealmSwift

class FavouritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var pokemon = [PokemonEntity]()

    @IBOutlet weak var favouriteTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getFavouritePokemon()
    }
    
    func getFavouritePokemon(){
        let realm = try!Realm()
        pokemon += Array(realm.objects(PokemonEntity.self).filter("isFavourite = True"))
        self.favouriteTableView.reloadData()
        
        print(pokemon.count)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "toFavouritePokemonSegue", sender: self)
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemon.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        cell.pokemonNameLabel?.text = pokemon[indexPath.row].name.capitalized
        cell.pokemonImageView.kf.setImage(with: URL(string: pokemon[indexPath.row].imageURL))
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "toFavouritePokemonSegue" {
            let destination = segue.destination as! PokemonViewController
            let selectedPokemon = pokemon[favouriteTableView.indexPathForSelectedRow?.row ?? 0]
            destination.pokemonURL = selectedPokemon.url
            destination.pokemonName = selectedPokemon.name
        }
    }

}
