//
//  CreateAccountViewController.swift
//  Pokedex
//
//  Created by Juan Sulca on 4/30/19.
//  Copyright © 2019 EPN. All rights reserved.
//

import UIKit
import FirebaseAuth

class CreateAccountViewController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        confirmPasswordTextField.delegate = self
    }
    
    @IBAction func createButtonPressed(_ sender: Any) {
        Auth.auth().createUser(withEmail: emailTextField.text!, password: passwordTextField.text!){(result, error) in
            if let _ = error {
                self.showAlertError(withMessage: error?.localizedDescription ?? "Error")
            } else {
                let mainViewController = self.storyboard?.instantiateViewController(withIdentifier: "UserInfoViewController") as! UserInfoViewController
                self.present(mainViewController, animated: true, completion: nil)
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if passwordTextField.text != confirmPasswordTextField.text {
            showAlertError(withMessage: "Passwords do not match!")
        }
    }
    
    func showAlertError(withMessage message: String){
        let alertController = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
